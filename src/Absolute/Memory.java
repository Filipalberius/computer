package Absolute;

public class Memory {
    private Word[] memory;
    private int size;

    public Memory(int size, WordFactory wordFactory) {
        if(size < 1) {
            throw new IllegalArgumentException("impossible size of memory");
        }

        this.memory = new Word[size];
        this.size = size;

        for(int i = 0; i < size; i++) {
            memory[i] = wordFactory.word("0");
        }
    }

    Word get(int index) {
        if(index < 0 || index > size) {
            throw new IndexOutOfBoundsException();
        }

        return memory[index];
    }
}