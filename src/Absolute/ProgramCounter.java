package Absolute;

public class ProgramCounter {
	private int index;
	
	public ProgramCounter() {
		index = 0;
	}
	
	public void set(int index) {
		this.index = index;
	}
	
	int get() {
		return index;
	}

	public void increment() {
		index++;
	}
}