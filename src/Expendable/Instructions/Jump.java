package Expendable.Instructions;

import Absolute.Instruction;
import Absolute.Memory;
import Absolute.ProgramCounter;

public class Jump implements Instruction{

    private int nextIndex;

    public Jump (int index){
        this.nextIndex = index;
    }

    @Override
    public void execute(Memory memory, ProgramCounter pc) {
        pc.set(nextIndex);
    }

    public String toString(){
        return "Jump to " + nextIndex;
    }
}