package Absolute;

public class Address implements Input {
    private int address;

    public Address(int address) {
        this.address = address;
    }

    int get() {
        return address;
    }

    public Word getWord(Memory memory) {
        return memory.get(address);
    }

    public String toString() {
        return "[" + address + "]";
    }
}