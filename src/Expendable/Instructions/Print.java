package Expendable.Instructions;

import Absolute.Input;
import Absolute.Memory;

public class Print implements StepMethod {
	private Input input;
	
	public Print(Input message) {
		this.input = message;
	}
	
	public void op(Memory memory) {
		System.out.println(input.getWord(memory).toString());
	}
	
	@Override
	public String toString() {
		return "Print " + input.toString();
	}
}