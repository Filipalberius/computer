package Expendable.Words;

import Absolute.Word;

public class ByteWord implements Word {
    private byte value;

    public ByteWord (String value) {
        try {
            this.value = Byte.parseByte(value);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("argument needs to be a number, or number too large for a byte");
        }
    }

    public void add(Word left, Word right) {
        this.value = (byte)(((ByteWord)left).value + ((ByteWord)right).value);
    }

    public void mul(Word left, Word right) {
        this.value = (byte)(((ByteWord)left).value * ((ByteWord)right).value);
    }

    /**
     * checks if tho words are equal
     * @param other the object for .this to be compared with
     * @return true if equal
     */
    @Override
    public boolean equals(Object other) {
        if(!(other instanceof Word))
            return false;
        else
            return value == ((ByteWord)other).value;
    }

    public void update(Word word) {
        this.value = ((ByteWord)word).value;
    }

    public String toString(){
        return String.valueOf(value);
    }
}