package Expendable.Words;

import Absolute.WordFactory;

public class LongWordFactory implements WordFactory {

    public LongWord word(String value){
        return new LongWord(value);
    }
}