package Absolute;

import java.util.List;

public class Computer {
    private Memory memory;
    private Program currentProgram;
    private ProgramCounter PC = new ProgramCounter();

    public Computer (Memory memory) { this.memory = memory; }

    public void load(Program program){ currentProgram = program; }

    public void run(){
        List<Instruction> instructions = currentProgram.getInstructions();

        try {
            while (PC.get() != -1) {
                instructions.get(PC.get()).execute(memory, PC);
            }
        } catch (ClassCastException e) {
            throw new ClassCastException("different types of words can not be used in the same program");
        } catch (IndexOutOfBoundsException e) {
            throw new IndexOutOfBoundsException("missing halt statement");
        }
    }
}