package Expendable.Instructions;

import Absolute.Instruction;
import Absolute.Memory;
import Absolute.ProgramCounter;

public class Halt implements Instruction{

    @Override
    public void execute(Memory memory, ProgramCounter PC) {
        PC.set(-1);
    }

    @Override
    public String toString(){
        return "Halt";
    }
}