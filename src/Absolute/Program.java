package Absolute;

import java.util.ArrayList;
import java.util.List;

public abstract class Program {
    private List<Instruction> instructions;


    public Program() {
        this.instructions = new ArrayList<>();
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("================================\n");
        for(int i = 0; i < instructions.size(); i++)
            stringBuilder.append(i + ": " + instructions.get(i).toString() + "\n");
        return stringBuilder.toString();
    }

    protected void add(Instruction instruction) {
        instructions.add(instruction);
    }

    List<Instruction> getInstructions() {
        return instructions;
    }

}