package Expendable.Instructions;

import Absolute.Input;
import Absolute.Instruction;
import Absolute.Memory;
import Absolute.ProgramCounter;

public class JumpEq implements Instruction{

    private int nextIndex;
    private Input left;
    private Input right;

    public JumpEq (int index, Input left, Input right){
        this.nextIndex = index;
        this.left = left;
        this.right = right;
    }

    @Override
    public void execute(Memory memory, ProgramCounter pc) {
        if (left.getWord(memory).equals(right.getWord(memory)))
            pc.set(nextIndex);
        else
            pc.increment();
    }

    public String toString(){
        return "Jump to " + nextIndex + " if " + left.toString() + " == " + right.toString();
    }
}