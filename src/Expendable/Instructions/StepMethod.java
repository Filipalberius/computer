package Expendable.Instructions;

import Absolute.Instruction;
import Absolute.Memory;
import Absolute.ProgramCounter;

public interface StepMethod extends Instruction {

    @Override
    default void execute(Memory memory, ProgramCounter pc) {
        op(memory);
        pc.increment();
    }

    void op(Memory memory);
}