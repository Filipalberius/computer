package Expendable.Words;

import Absolute.WordFactory;

public class ByteWordFactory implements WordFactory {

    public ByteWord word(String value){
        return new ByteWord(value);
    }
}