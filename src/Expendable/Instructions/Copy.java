package Expendable.Instructions;

import Absolute.Address;
import Absolute.Input;
import Absolute.Memory;

public class Copy implements StepMethod {
    private Input input;
    private Address address;

    public Copy(Input input, Address address) {
        this.input = input;
        this.address = address;
    }

    @Override
    public String toString() {
        return "Copy " + input.toString() + " to " + address.toString();
    }

    public void op(Memory memory) {
        address.getWord(memory).update(input.getWord(memory));
    }
}