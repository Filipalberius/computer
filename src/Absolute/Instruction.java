package Absolute;

public interface Instruction {

	void execute(Memory memory, ProgramCounter pc);

	String toString();
}