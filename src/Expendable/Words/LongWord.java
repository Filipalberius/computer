package Expendable.Words;


import Absolute.Word;

public class LongWord implements Word {

    private long value;

    public LongWord(String value) {
        try {
            this.value = Long.parseLong(value);
        } catch(IllegalArgumentException e) {
            throw new IllegalArgumentException("argument needs to be a number, or number is too large for a long");
        }
    }

    public void add(Word left, Word right) {
        this.value = ((LongWord)left).value + ((LongWord)right).value;
    }

    public void mul(Word left, Word right) {
        this.value = ((LongWord)left).value * ((LongWord)right).value;
    }

    @Override
    public boolean equals(Object other) {
        if(!(other instanceof Word))
            return false;
        else
            return value == ((LongWord)other).value;
    }

    public void update(Word word) {
        this.value = ((LongWord)word).value;
    }

    public String toString(){
        return String.valueOf(value);
    }
}