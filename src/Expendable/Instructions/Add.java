package Expendable.Instructions;

import Absolute.Address;
import Absolute.Input;
import Absolute.Word;

public class Add extends BinOp {

    public Add (Input left, Input right, Address destination){
        super(left, right, destination);
    }

    protected void binOp(Word left, Word right, Word destination) {
        destination.add(left, right);
    }

    public String operator(){
        return "Add";
    }
}