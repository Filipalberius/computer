package Expendable.Instructions;


import Absolute.Address;
import Absolute.Input;
import Absolute.Memory;
import Absolute.Word;

public abstract class BinOp implements StepMethod {
	private Input left, right;
	private Address destination;
	
	public BinOp(Input left, Input right, Address destination) {
		this.left = left;
		this.right = right;
		this.destination = destination;
	}
	
	public void op(Memory memory) {
		binOp(left.getWord(memory), right.getWord(memory), destination.getWord(memory));
	}

	public String toString() {
	    return operator() + " " + left.toString() + " and " + right.toString() + " into " + destination.toString();
    }

    protected abstract String operator();
	
	protected abstract void binOp(Word left, Word right, Word destination);
}