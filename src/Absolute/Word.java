package Absolute;

public interface Word extends Input {
    default Word getWord(Memory memory){
        return this;
    }

    void add(Word left, Word right);

    void mul(Word left, Word right);

    void update(Word value);

    String toString();

    boolean equals(Object other);
}