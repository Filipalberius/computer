package Absolute;

public interface Input {
    Word getWord(Memory memory);
}